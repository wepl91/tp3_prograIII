package frontend;

import java.awt.EventQueue;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.UIManager;

import backend.Funciones;
import backend.Jugador;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class VisualizadorEquipo {

	private JFrame frame;

	public static void main(String[] args) {
		
		try {
			
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
			
		} catch (Exception e) { }
		
		EventQueue.invokeLater(new Runnable() {
		
			public void run() {
			
				try {
				
					VisualizadorEquipo window = new VisualizadorEquipo();
					window.frame.setVisible(true);
				
				} catch (Exception e) {
				
					e.printStackTrace();
				
				}
			}
		});
	}

	public VisualizadorEquipo() throws IOException {
		
		initialize();
		
	}

	private void initialize() throws IOException {
		
		DefaultListModel<String> model = new DefaultListModel<>();
		ArrayList <Jugador> equipoTitular = Funciones.obtenerListaJugadores("/home/walter/Documentos/equipoTitular.txt");
		BufferedImage img = ImageIO.read(new File("/home/walter/Imágenes/copa.jpg"));
		frame = new JFrame();
		frame.setBounds(100, 100, 400, 270);
		frame.setTitle("Equipo Titular");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		model.addElement("");
		for (Jugador jugador : equipoTitular) {
			
			model.addElement((jugador.getPosicion() + " : " + jugador.getNombre() + " , Nivel: " + jugador.getNivel()));
			
		}
		frame.getContentPane().setLayout(null);
		
		JList <String> list = new JList<>(model);
		list.setBackground(new Color(128, 128, 128));
		list.setBounds(0, 0, 250, 245);		
		frame.getContentPane().add(list);
		
		JButton btnClose = new JButton("Close");
		btnClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		btnClose.setBounds(267, 209, 115, 25);
		frame.getContentPane().add(btnClose);
		
		JLabel backgroud = new JLabel(new ImageIcon(img));
		backgroud.setBounds(-62, -114, 771, 471);
		frame.getContentPane().add(backgroud);
	}
}
