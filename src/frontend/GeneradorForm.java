package frontend;

import backend.Funciones;
import backend.Jugador;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.UIManager;

import com.google.gson.Gson;

import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class GeneradorForm {

	private JFrame frmGeneradoJson;
	private JTextField txtNombre;
	private JTextField txtPosicion;
	private JTextField txtNivel;
	private JTextField txtIncompatibles;

	public static void main(String[] args) {
		
		try {
			
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
			
		} catch (Exception e) { }
		
		EventQueue.invokeLater(new Runnable() {
			
			public void run() {
				
				try {
					
					GeneradorForm window = new GeneradorForm();
					window.frmGeneradoJson.setVisible(true);
					
				} catch (Exception e) {
					
					e.printStackTrace();
					
				}
			}
		});
	}

	public GeneradorForm() throws IOException {
		
		initialize();
		
	}

	private void initialize() throws IOException {
	
		final Gson gson = new Gson();
		final ArrayList <Jugador> listaJugadores = new ArrayList<>();
		
		frmGeneradoJson = new JFrame();
		frmGeneradoJson.getContentPane().setBackground(new Color(119, 136, 153));
		frmGeneradoJson.setTitle("Generador JSON");
		frmGeneradoJson.setBounds(900, 100, 262, 312);
		frmGeneradoJson.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmGeneradoJson.getContentPane().setLayout(null);
		
		txtNombre = new JTextField();
		txtNombre.setBounds(127, 6, 122, 27);
		frmGeneradoJson.getContentPane().add(txtNombre);
		txtNombre.setColumns(10);
		
		txtPosicion = new JTextField();
		txtPosicion.setBounds(127, 45, 122, 27);
		frmGeneradoJson.getContentPane().add(txtPosicion);
		txtPosicion.setColumns(10);
		
		txtNivel = new JTextField();
		txtNivel.setBounds(127, 83, 122, 27);
		frmGeneradoJson.getContentPane().add(txtNivel);
		txtNivel.setColumns(10);
		
		txtIncompatibles = new JTextField();
		txtIncompatibles.setBounds(127, 122, 122, 27);
		frmGeneradoJson.getContentPane().add(txtIncompatibles);
		txtIncompatibles.setColumns(10);
		
		JLabel lblNombre = new JLabel("Nombre:");
		lblNombre.setBounds(19, 12, 60, 15);
		frmGeneradoJson.getContentPane().add(lblNombre);
		
		JLabel lblPosicin = new JLabel("Posición:");
		lblPosicin.setBounds(19, 51, 60, 15);
		frmGeneradoJson.getContentPane().add(lblPosicin);
		
		JLabel lblNivelDeJuego = new JLabel("Nivel de juego:");
		lblNivelDeJuego.setBounds(19, 89, 95, 15);
		frmGeneradoJson.getContentPane().add(lblNivelDeJuego);
		
		JLabel lblIncompatibles = new JLabel("Incompatibles:");
		lblIncompatibles.setBounds(19, 128, 95, 15);
		frmGeneradoJson.getContentPane().add(lblIncompatibles);
		
		JButton btnAgregarJugador = new JButton("Agregar Jugador");
		btnAgregarJugador.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				
				String nombre = txtNombre.getText().toUpperCase();
				String posicion = txtPosicion.getText().toUpperCase();
				String incompatibles[] = txtIncompatibles.getText().toUpperCase().split(",");
				
				if (Funciones.verificarEntradaVacia(nombre) || Funciones.verificarEntradaVacia(posicion) || Funciones.verificarEntradaVacia(txtNivel.getText()) || !Funciones.esNumero(txtNivel.getText())) {
					
					JOptionPane.showMessageDialog(null,"Falta algún dato importante del jugador que desea cargar, por favor verifique todos los datos.");
					
				} else {
					
					int nivel = Integer.parseInt(txtNivel.getText());
					Jugador jugador = new Jugador (nombre, posicion, nivel, incompatibles);
					listaJugadores.add(jugador);
					
					txtNombre.setText("");
					txtNombre.setCursor(null);
					txtPosicion.setText("");
					txtNivel.setText("");
					txtIncompatibles.setText("");
				
				}
				
				
				
			}
			
		});
		btnAgregarJugador.setBounds(113, 161, 136, 27);
		frmGeneradoJson.getContentPane().add(btnAgregarJugador);
		
		JButton btnGenerarJson = new JButton("Generar JSON");
		btnGenerarJson.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				
				final String jsonString = gson.toJson(listaJugadores);
				FileWriter fileWriter;
				
				try {
				
					fileWriter = new FileWriter("/home/walter/Documentos/equipo.txt");
					fileWriter.write(jsonString);
					JOptionPane.showMessageDialog(null,"Archivo generado y guardado exitosamente!");
					fileWriter.flush();
					fileWriter.close();
					frmGeneradoJson.setVisible(false);
					
				} catch (IOException e1) {
					
					e1.printStackTrace();
				
				}
				
			}
		});
		btnGenerarJson.setBounds(113, 200, 136, 27);
		frmGeneradoJson.getContentPane().add(btnGenerarJson);
		
		JButton btnClose = new JButton("Close");
		btnClose.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {frmGeneradoJson.setVisible(false);}
			
		});
		btnClose.setBounds(113, 239, 136, 27);
		frmGeneradoJson.getContentPane().add(btnClose);
		
		
	}
}
