package frontend;

import backend.Funciones;
import backend.Jugador;

import java.awt.EventQueue;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.UIManager;

import com.google.gson.Gson;

import javax.imageio.ImageIO;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JList;

import java.awt.Color;

public class MainForm {

	private JFrame mainFrame;
		
	public static void main(String[] args) {
		
		try {
			
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
			
		} catch (Exception e) { }
		
		EventQueue.invokeLater(new Runnable() {
			
			public void run() {
				
				try {
					
					MainForm window = new MainForm();
					window.mainFrame.setVisible(true);
					
				} catch (Exception e) {
					
					e.printStackTrace();
					
				}
			}
		});
	}

	public MainForm() throws IOException {
		initialize();
	}

	private void initialize() throws IOException {
		
		final Gson gson = new Gson();
		DefaultListModel<String> model = new DefaultListModel<>();
		ArrayList <Jugador> listaJugadores = Funciones.obtenerListaJugadores("/home/walter/Documentos/equipo.txt");
		BufferedImage img = ImageIO.read(new File("/home/walter/Imágenes/background.jpg"));
		
		mainFrame = new JFrame();
		mainFrame.setAlwaysOnTop(true);
		mainFrame.setBounds(100, 100, 700, 453);
		mainFrame.setTitle("Rumbo al mundial!");
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JButton btnLeerJson = new JButton("Leer Json");
		btnLeerJson.setBounds(553, 353, 126, 25);
		btnLeerJson.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				 
				model.addElement("Jugadores:");
				model.addElement("");
				for (Jugador jugador : listaJugadores) {
					
					model.addElement((jugador.getNombre()));
					
				}
				
				btnLeerJson.setEnabled(false);
				
			}
		});
		mainFrame.getContentPane().setLayout(null);
		mainFrame.getContentPane().add(btnLeerJson);
		
		JButton btnGenerador = new JButton("Generar Json");
		btnGenerador.setBounds(553, 314, 126, 27);
		btnGenerador.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				GeneradorForm.main(null);
			
			}
		});
		mainFrame.getContentPane().add(btnGenerador);
		
		JButton btnClose = new JButton("Close");
		btnClose.setBounds(120, 390, 126, 27);
		btnClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				System.exit(0);
			
			}
		});
		mainFrame.getContentPane().add(btnClose);
		
		JList <String> list = new JList<>(model);
		list.setBackground(new Color(128, 128, 128));
		list.setBounds(0, 0, 108, 435);		
		mainFrame.getContentPane().add(list);
		
		JButton btnNewButton = new JButton("Generar equipo");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				ArrayList <Jugador> equipoOficial = Funciones.generarEquipoOptimo(listaJugadores);
				
				final String jsonString = gson.toJson(equipoOficial);
				FileWriter fileWriter;
				
				try {
					
					fileWriter = new FileWriter("/home/walter/Documentos/equipoTitular.txt");
					fileWriter.write(jsonString);
					fileWriter.flush();
					fileWriter.close();
					mainFrame.setVisible(false);
					VisualizadorEquipo.main(null);
					
				} catch (IOException e1) {
					
					e1.printStackTrace();
				
				}
				
			}
		});
		btnNewButton.setBounds(553, 390, 126, 27);
		mainFrame.getContentPane().add(btnNewButton);

		JLabel backgroud = new JLabel(new ImageIcon(img));
		backgroud.setBounds(10, -26, 771, 471);
		mainFrame.getContentPane().add(backgroud);
		
		
		
		
		
		
	}
}
