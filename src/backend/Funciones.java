package backend;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import javax.swing.JOptionPane;

import com.google.gson.Gson;

public class Funciones {
	
	/*
	 * Funcion que lee el archivo Json
	 */
	@SuppressWarnings("unchecked")
	public static ArrayList <Jugador> leerJson (String archivoJson) {
		Gson gson = new Gson();
		ArrayList <Jugador> ret = null; 
		
		try {
			BufferedReader br = new BufferedReader(new FileReader (archivoJson));
			ret = gson.fromJson(br, ArrayList.class);
			
		} catch (Exception e) { }
			
		return ret;
	}
	
	/*
	 * Funcion que obtiene la lista de jugadores a partir de un archivo Json
	 */
	public static ArrayList <Jugador> obtenerListaJugadores(String path) {
		
		Gson gson = new Gson();
		ArrayList<Jugador> equipoAux= Funciones.leerJson(path);
		ArrayList<Jugador> equipo = new ArrayList<>();
		for (int i = 0; i < equipoAux.size(); i++) {
			String json = gson.toJson(equipoAux.get(i));
			Jugador jugador = gson.fromJson(json, Jugador.class);
			
			equipo.add(jugador);
		}
		
		return equipo;
	
	}
	
	/*
	 * Funcion que genera el mejor equipo posible
	 */
	public static ArrayList <Jugador> generarEquipoOptimo (ArrayList <Jugador> listaJugadores) {
		
		ArrayList <Jugador> arqueros = new ArrayList <>();
		ArrayList <Jugador> defensores = new ArrayList <>();
		ArrayList <Jugador> mediocampistas = new ArrayList <>();
		ArrayList <Jugador> delanteros = new ArrayList <>();
		ArrayList <Jugador> equipoOptimo = new ArrayList<>();
		
		separarPorPosicion(listaJugadores, arqueros, defensores, mediocampistas, delanteros);
		ordenarJugadoresPorPosicion(arqueros, defensores, mediocampistas, delanteros);
		agregarJugadores(arqueros, defensores, mediocampistas, delanteros, equipoOptimo);
		
		return equipoOptimo;
		
	}
	
	/*
	 * Funcion que agrega los distinto tipos de jugadores al equipo oficial
	 */
	private static void agregarJugadores(ArrayList<Jugador> arqueros, ArrayList<Jugador> defensores,
			ArrayList<Jugador> mediocampistas, ArrayList<Jugador> delanteros, ArrayList<Jugador> equipoOptimo) {
		
		try {
		
			//Se agrega como primer jugador el primer arquero del Array ya que es el arquero con mayor nivel
			equipoOptimo.add(arqueros.get(0));
			
			agregarDefensores(defensores, equipoOptimo);
			agregarMediocampistas(mediocampistas, equipoOptimo);
			agregarDelanteros(delanteros, equipoOptimo);
		
		} catch (Exception e) {
			
			JOptionPane.showMessageDialog(null, "La lista brindada no posee la cantidad suficiente de jugadores.");
			
		}
		
	}
	
	/*
	 * Funcion que separa todos los jugadores por posicion y los ordena por nivel
	 */
	private static void ordenarJugadoresPorPosicion(ArrayList<Jugador> arqueros, ArrayList<Jugador> defensores,
		ArrayList<Jugador> mediocampistas, ArrayList<Jugador> delanteros) {
		
		ordenarJugadores(arqueros, comparadorPorNivel());
		ordenarJugadores(defensores, comparadorPorNivel());
		ordenarJugadores(mediocampistas, comparadorPorNivel());
		ordenarJugadores(delanteros, comparadorPorNivel());
		
	}
	
	/*
	 * Funcion que agrega los 3 delanteros al equipo
	 */
	static void agregarDelanteros(ArrayList<Jugador> delanteros, ArrayList<Jugador> equipoOptimo) {
		
		int sum = 0;
		
		try {
			
			for (Jugador jugador : delanteros) {
				
				if (!esIncompatible(jugador, equipoOptimo) && !equipoOptimo.contains(jugador)) {
					equipoOptimo.add(jugador);
					sum +=1;
						
				}
				if (sum == 3) {
					return;
				}
					
			}
			
		} catch (Exception e) {
			
			JOptionPane.showMessageDialog(null,"Por problemas de incopatibilidad con otros jugadores, no hay suficientes delanteros en la lista para generar los titulares");
			
		}
		
			
		
	}
	
	/*
	 * Funcion que agrega los 3 mediocampistas
	 */
	static void agregarMediocampistas(ArrayList<Jugador> mediocampistas, ArrayList<Jugador> equipoOptimo) {
		
		int sum = 0;
		
		try {
			
			for (Jugador jugador : mediocampistas) {
				
				if (!esIncompatible(jugador, equipoOptimo) && !equipoOptimo.contains(jugador)) {
						
					equipoOptimo.add(jugador);
					sum += 1;
						
				}
				
				if (sum == 3) {
					return;
				}
					
			}
			
		} catch (Exception e) {
			
			JOptionPane.showMessageDialog(null,"Por problemas de incopatibilidad con otros jugadores, no hay suficientes mediocampistas en la lista para generar los titulares");
			
		}
			
		
			
		
	}
	
	/*
	 * Funcion que agrega los 4 defensores
	 */
	static void agregarDefensores(ArrayList<Jugador> defensores, ArrayList<Jugador> equipoOptimo) {
		
		int sum = 0;
		
		try	{
			
			for (Jugador jugador : defensores) {
				
				if (!esIncompatible(jugador, equipoOptimo) && !equipoOptimo.contains(jugador)) {
						
					equipoOptimo.add(jugador);
					sum +=1;
				}
				
				if (sum == 4) {
					
					return;
					
				}
					
			}
			
		} catch (Exception e) {
			
			JOptionPane.showMessageDialog(null,"Por problemas de incopatibilidad con otros jugadores, no hay suficientes defensores en la lista para generar los titulares");
		
		}
		
			
		
	}
	
	/*
	 * Función que separa toda la lista de jugadores inicial en 4 listas : Arqueros, Defensores, Mediocampistas y Delanteros
	 */
	private static void separarPorPosicion(ArrayList<Jugador> listaJugadores, ArrayList<Jugador> arqueros,
			ArrayList<Jugador> defensores, ArrayList<Jugador> mediocampistas, ArrayList<Jugador> delanteros) {
		
		for (Jugador jugador : listaJugadores ) {
			
			try {
			
				if (jugador.getPosicion().equals("ARQUERO")) {
					
					arqueros.add(jugador);
					
				}
				
				if (jugador.getPosicion().equals("DEFENSOR")) {
					
					defensores.add(jugador);
					
				}
				
				if (jugador.getPosicion().equals("MEDIOCAMPISTA")) {
		
					mediocampistas.add(jugador);
		
				}
				
				if (jugador.getPosicion().equals("DELANTERO")) {
		
					delanteros.add(jugador);
		
				}
				
			} catch (Exception e) {
				
				JOptionPane.showMessageDialog(null, "La lista brindada no posee la cantidad suficiente de jugadores.");
				
			}
			
			
			
		}
		
	}
	
	/*
	 * Funcion que compara 2 jugadores y devuelve el que tiene mayor nivel
	 */
	private static Comparator <Jugador> comparadorPorNivel() {
	
		return (uno, otro) -> otro.getNivel() - uno.getNivel();	
	
	}
	
	/*
	 * Funcion que ordena la lista de jugadores de mayor a menor nivel
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static void ordenarJugadores(ArrayList<Jugador> jugadores, Comparator comparador) {
		
		Collections.sort(jugadores, comparador);
	
	}
	
	/*
	 * Funcion que checkea si un jugador se encuentra en los incompatibles de alguno de los jugadores en el equipo oficial
	 */
	public static boolean esIncompatible (Jugador jugador, ArrayList <Jugador> equipoOptimo) {
		
		
		for (Jugador jugadorEnEquipo : equipoOptimo) {
			
			if ((jugadorEnEquipo.getIncompatibles().contains(jugador.getNombre())) || (jugador.getIncompatibles().contains(jugadorEnEquipo.getNombre()))) {
				return true;
			}
						
		}
		return false;
		
		
		
	}
	
	/*
	 * Funcion que verifica si una entrada es vacia o no
	 */
	public static boolean verificarEntradaVacia (String entrada) {
		
		if (entrada.equals("")) {
			
			return true;
			
		}
		
		return false;
		
	}
	
	/*
	 * Funcion que verifica si una entrada es numerica o no
	 */
	public static boolean esNumero (String entrada) {
		
		try {
			
			Integer.parseInt(entrada);
			return true;
			
		} catch (NumberFormatException nfe) {
			
			return false;
			
		}
	}
	
	/*
	 * Funcion que calcula el nivel total de un equipo pasado por parametro
	 */
	public static int calcularNivel (ArrayList <Jugador> equipo) {
		
		int sum = 0;
		
		for (Jugador jugador : equipo) {
			
			sum += jugador.getNivel();
			
		}
		
		return sum;
		
	}
}
