package backend;
import backend.Jugador;
import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

public class Assert {
	
	ArrayList <Jugador> listaJugadoresVacia = new ArrayList<>();
	ArrayList <Jugador> equipoOptimo = new ArrayList<>();
	
	@Before
	public void construirListaChica() {
		
		String incompatibles[] = {""};
		
		listaJugadoresVacia.add(new Jugador ("MESSI","DELANTERO",9, incompatibles));
		
	}
	
	@Test
	//Generacion de un equipo con una lista con solo 1 jugador
	public void generarEquipoVacio() {
		
		Funciones.generarEquipoOptimo(listaJugadoresVacia);
		
	}
	
	@Test
	//Intentar agregar un jugador de una lista vacia al equipo titular
	public void agregarVacio() {
		
		ArrayList <Jugador> listaVacia = new ArrayList<>();
		Funciones.agregarDefensores(listaVacia, equipoOptimo);
		Funciones.agregarDelanteros(listaVacia, equipoOptimo);
		Funciones.agregarMediocampistas(listaVacia, equipoOptimo);
		
	}
	
	@Test
	public void entradaVacia() {
		
		Funciones.verificarEntradaVacia("");
		
	}
	
	@Test
	public void verificarEsNumero() {
		
		assertFalse(Funciones.esNumero("a"));
		assertFalse(Funciones.esNumero(""));
		assertTrue(Funciones.esNumero("1"));
		
	}
	
	@Test
	//Verificar si es incompatible un jugador con si mismo
	public void verificarEsIncompatible() {
		
		String incompatibles[] = {"MESSI"};
		Jugador jugador = new Jugador("MESSI", "DELANTERO", 9, incompatibles);
		assertFalse(Funciones.esIncompatible(jugador, equipoOptimo));
		
	}
	
	
}
