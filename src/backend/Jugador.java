package backend;

import java.util.ArrayList;

public class Jugador {

	String nombre;
	String posicion;
	int nivel;
	String incompatibles[];
	
	public Jugador (String nombre, String posicion, int nivel, String incompatibles[]) {
		
		this.nombre = nombre;
		this.posicion = posicion;
		this.nivel = nivel;
		this.incompatibles = incompatibles;
		
	}

	/*
	 * Getter del nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/*
	 * Getter de la posicion
	 */
	public String getPosicion() {
		return posicion;
	}

	/*
	 * Getter del nivel
	 */
	public int getNivel() {
		return nivel;
	}
	
	/*
	 * Getter de lista de incompatibles pasado de Array a ArrayList
	 */
	public ArrayList<String> getIncompatibles() {
		
		ArrayList <String> incompatiblesList = new ArrayList<>();
		
		for (String jugador : incompatibles) {
			incompatiblesList.add(jugador.toUpperCase());
		}
		return incompatiblesList;
	}

}
